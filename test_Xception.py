import csv
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import imutils
import cv2
import numpy as np
import sys
import dlib 
import os

emotion_model_path = 'models/_mini_XCEPTION.85-0.96.hdf5'
image_test_path = 'images/vn_celeb_face_recognition/test'
write_result_path = 'result/result.csv'

emotion_classifier = load_model(emotion_model_path, compile=False)


def predict_test():
    if os.path.exists(write_result_path):
        os.remove(write_result_path)
        
    print("[+] Predict test data....")
    with open(write_result_path, mode='a') as csv_file:
        fieldnames = ['image', 'label']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
    
    file_names = [os.path.join(image_test_path, f) 
                      for f in os.listdir(image_test_path) if f.endswith(".png")]
    
    for f in file_names:
        image = cv2.imread(f)
        image = cv2.resize(image, (100, 100))
        image = image.astype("float") / 255.0
        data = img_to_array(image)
        data = np.expand_dims(data, axis=0)
        preds = emotion_classifier.predict(data)[0]
        sort_prob = np.argsort(preds)
        print(f)
        split_name = f.split('/')[-1]
        result = str(sort_prob[-1]) + ' ' + str(sort_prob[-2]) + ' ' + str(sort_prob[-3]) + ' '  +  str(sort_prob[-4]) + ' ' + str(sort_prob[-5])
        print(np.max(preds))
        with open(write_result_path, mode='a') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow([split_name, result])
            csv_file.close
        # print(split_name)
    
    print("[+] Predict test data finished, write result to: " + write_result_path)

if __name__=='__main__':
    # test_path = sys.argv[1]
    predict_test()