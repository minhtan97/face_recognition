import numpy as np
import sys
import os
from PIL import ImageFile
import cv2
import dlib
import pandas as pd
import skimage.data
import skimage.transform
from sklearn import svm
from sklearn.externals import joblib
from sklearn import metrics
from matplotlib import pyplot as plt

# ImageFile.LOAD_TRUNCATED_IMAGES=True

predictor_model = "./weight/shape_predictor_68_face_landmarks.dat"
face_recognition_model = './weight/dlib_face_recognition_resnet_model_v1.dat'
image_train_path = 'images/vn_celeb_face_recognition/train'
file_path = 'images/vn_celeb_face_recognition/train.csv'
save_path = 'features/train/'

sp = dlib.shape_predictor(predictor_model)
facerec = dlib.face_recognition_model_v1(face_recognition_model)

def save_feature(save_path, feature):    
    os.makedirs(os.path.dirname(save_path), exist_ok=True)

    print("[+]Save extracted feature to file : ", save_path)
    np.save(save_path, feature)

def feature_extraction():
    column_names = ['image', 'label']
    data_csv = pd.read_csv(file_path, header = 0, names = column_names)
    data = []
    label = []
    for i in range(data_csv.shape[0]):
        if os.path.isfile(os.path.join(image_train_path, data_csv['image'][i])):
            image = cv2.imread(image_train_path + '/' + data_csv['image'][i])
            image = cv2.resize(image, (100, 100))
            shape = sp(image, dlib.rectangle(int(0),int(0),int(image.shape[1] - 1),int(image.shape[0] - 1)))
            print(i)
#             print(shape.parts()[36:42])
            face_chip = dlib.get_face_chip(image, shape)
#             plt.imshow(image)
#             plt.show()
            # name = data_csv['image'][i].split('.')[0]
            face_descriptor = facerec.compute_face_descriptor(face_chip)
            # save_feature(save_path + name, face_descriptor)
            data.append(face_descriptor)
            label.append(data_csv['label'][i])
    return np.array(data), np.array(label)      

def save_model(model, name):
    if not os.path.exists('models'):
        os.makedirs('models')
    os.chdir('models')
    file_name = name + ".joblib"
    print("[+] Saving model to file : " ,file_name)
    joblib.dump(model, file_name)

def accuracy(clf, images, labels):
    x_pred = clf.predict(images)
    print("Accuracy:",metrics.accuracy_score(labels, x_pred))

def rbf_svm(X_train, y_train):
    print("[!] SVM data...")
    clf = svm.SVC(kernel='linear').fit(X_train, y_train)
    print("[+] Finished")
    return clf
            
if __name__=="__main__":
    # file_path = sys.argv[1]
    print("Load data...")
    images, labels = feature_extraction()
    print("Done load data...")
    clf = rbf_svm(images, labels)
    save_model(clf, "model")
    accuracy(clf, images, labels)