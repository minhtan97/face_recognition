from sklearn import svm
from sklearn.externals import joblib
import csv
import numpy as np
import sys
import os
from PIL import ImageFile
import cv2
import dlib
import pandas as pd
import skimage.data
import skimage.transform
from sklearn import metrics

predictor_model = "./weight/shape_predictor_68_face_landmarks.dat"
face_recognition_model = './weight/dlib_face_recognition_resnet_model_v1.dat'
image_test_path = 'images/vn_celeb_face_recognition/test'
svm_model = 'models/model.joblib'
write_result_path = 'result/result.csv'

sp = dlib.shape_predictor(predictor_model)
facerec = dlib.face_recognition_model_v1(face_recognition_model)

def predict_test():
    if os.path.exists(write_result_path):
        os.remove(write_result_path)
    print("[+] Load model....")
    clf = joblib.load(svm_model)
    print("[+] Load model finished")
    print("[+] Predict test data....")
    with open(write_result_path, mode='a') as csv_file:
        fieldnames = ['image', 'label']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
    
    file_names = [os.path.join(image_test_path, f) 
                      for f in os.listdir(image_test_path) if f.endswith(".png")]
    
    for f in file_names:
        image = cv2.imread(f)
        image = cv2.resize(image, (100, 100))
        shape = sp(image, dlib.rectangle(int(0),int(0),int(image.shape[1] - 1),int(image.shape[0] - 1)))
        print(i)
        # face_chip = dlib.get_face_chip(image, shape)
        face_descriptor = facerec.compute_face_descriptor(image, shape)
        data = np.array(face_descriptor)

        split_name = f.split('/')[-1]
        result = str(clf.predict(data)[0]) + ' ' str(clf.predict(data)[1]) + ' ' str(clf.predict(data)[2]) + ' ' str(clf.predict(data)[3]) + ' ' str(clf.predict(data)[4])
        with open(write_result_path, mode='a') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow([split_name, result])
            csv_file.close
        # print(split_name)
    
    print("[+] Predict test data finished, write result to: " + write_result_path)

if __name__=='__main__':
    # test_path = sys.argv[1]
    predict_test()
