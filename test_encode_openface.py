import numpy as np
import os
import matplotlib.pyplot as plt
import cv2
from sklearn.decomposition import PCA
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
from imageio import imread
from skimage.transform import resize
from scipy.spatial import distance
from keras.models import load_model
import pandas as pd
from tqdm import tqdm
import dlib

predictor_model = "./weight/shape_predictor_5_face_landmarks.dat"
sp = dlib.shape_predictor(predictor_model)

model_path = './weight/facenet_keras.h5'
model = load_model(model_path)

def prewhiten(x):
    if x.ndim == 4:
        axis = (1, 2, 3)
        size = x[0].size
    elif x.ndim == 3:
        axis = (0, 1, 2)
        size = x.size
    else:
        raise ValueError('Dimension should be 3 or 4')

    mean = np.mean(x, axis=axis, keepdims=True)
    std = np.std(x, axis=axis, keepdims=True)
    std_adj = np.maximum(std, 1.0/np.sqrt(size))
    y = (x - mean) / std_adj
    return y

def l2_normalize(x, axis=-1, epsilon=1e-10):
    output = x / np.sqrt(np.maximum(np.sum(np.square(x), axis=axis, keepdims=True), epsilon))
    return output

def load_and_align_images(filepaths, margin,image_size = 160):
    
    aligned_images = []
    for filepath in filepaths:
        img = cv2.imread(filepath)
        img = cv2.resize(img, (image_size, image_size))
        img = img/255.
#         img = imread(filepath)
#         aligned = resize(img, (image_size, image_size))
#         print(aligned.shape)
        # shape = sp(img, dlib.rectangle(int(0),int(0),int(img.shape[1] - 1),int(img.shape[0] - 1)))
#         print(i)
        # face_chip = dlib.get_face_chip(img, shape)
        # face_chip = cv2.resize(face_chip, (image_size, image_size))
        aligned_images.append(img)
    return np.array(aligned_images)

def calc_embs(filepaths, margin=10, batch_size=512):
    pd = []
    for start in tqdm(range(0, len(filepaths), batch_size)):
        # aligned_images = prewhiten(load_and_align_images(filepaths[start:start+batch_size], margin))
        aligned_images = load_and_align_images(filepaths[start:start+batch_size], margin)
        pd.append(model.predict_on_batch(aligned_images))
    embs = l2_normalize(np.concatenate(pd))

    return embs

test_path = './images/vn_celeb_face_recognition/test/'
train_path = './images/vn_celeb_face_recognition/train/'

train_df = pd.read_csv('./images/vn_celeb_face_recognition/train.csv')
test_df = pd.read_csv('./images/vn_celeb_face_recognition/sample_submission.csv')

train_embs = calc_embs([os.path.join(train_path, f) for f in train_df.image.values])
np.save("train_embs.npy", train_embs)
# train_embs = np.load("train_embs.npy")

test_embs = calc_embs([os.path.join(test_path, f) for f in test_df.image.values])
np.save("test_embs.npy", test_embs)

# indices which belong to each label
label2idx = []

for i in tqdm(range(1000)):
    label2idx.append(np.asarray(train_df[train_df.label == i].index))

import matplotlib.pyplot as plt

match_distances = []
for i in range(1000):
    ids = label2idx[i]
    distances = []
    for j in range(len(ids) - 1):
        for k in range(j + 1, len(ids)):
            distances.append(distance.euclidean(train_embs[ids[j]], train_embs[ids[k]]))
    match_distances.extend(distances)
    
unmatch_distances = []
for i in range(1000):
    ids = label2idx[i]
    distances = []
    for j in range(5):
        idx = np.random.randint(train_embs.shape[0])
        while idx in label2idx[i]:
            idx = np.random.randint(train_embs.shape[0])
        distances.append(distance.euclidean(train_embs[ids[np.random.randint(len(ids))]], train_embs[idx]))
    unmatch_distances.extend(distances)
    
_,_,_=plt.hist(match_distances,bins=100)
_,_,_=plt.hist(unmatch_distances,bins=100,fc=(1, 0, 0, 0.5))

threshold = 1.1

for i in tqdm(range(len(test_df.image))):
    distances = []
    for j in range(1000):
        distances.append(np.min([distance.euclidean(test_embs[i], train_embs[k]) for k in label2idx[j]]))
    distances.append(threshold)
    test_df.loc[i].label = ' '.join([str(p) for p in np.argsort(distances)[:5]])

test_df.to_csv("sub.csv", index=False)