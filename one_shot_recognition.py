import numpy as np
import sys
import os
from PIL import ImageFile
import cv2
import dlib
import pandas as pd
import skimage.data
import skimage.transform
from sklearn import svm
from sklearn.externals import joblib
from sklearn import metrics
from matplotlib import pyplot as plt
import csv

# ImageFile.LOAD_TRUNCATED_IMAGES=True

predictor_model = "./weight/shape_predictor_68_face_landmarks.dat"
face_recognition_model = './weight/dlib_face_recognition_resnet_model_v1.dat'
image_train_path = 'images/vn_celeb_face_recognition/train'
file_path = 'images/vn_celeb_face_recognition/train.csv'
image_test_path = 'images/vn_celeb_face_recognition/test'
write_result_path = 'result/result.csv'
save_path_train = 'features/train/'
save_path_test = 'features/test/'
scale = 40

sp = dlib.shape_predictor(predictor_model)
facerec = dlib.face_recognition_model_v1(face_recognition_model)

def save_feature(save_path, feature):    
    os.makedirs(os.path.dirname(save_path), exist_ok=True)

    print("[+]Save extracted feature to file : ", save_path)
    np.save(save_path, feature)

def feature_extraction():
    column_names = ['image', 'label']
    data_csv = pd.read_csv(file_path, header = 0, names = column_names)
    data = []
    label = []
    # data_csv.shape[0]
    for i in range(data_csv.shape[0]):
        if os.path.isfile(os.path.join(image_train_path, data_csv['image'][i])):
            image = cv2.imread(image_train_path + '/' + data_csv['image'][i])
            image = cv2.resize(image, (150, 150))
            shape = sp(image, dlib.rectangle(int(0),int(0),int(image.shape[1] - 1),int(image.shape[0] - 1)))
            print(i)
#             print(shape.parts()[36:42])
            face_chip = dlib.get_face_chip(image, shape)
#             plt.imshow(image)
#             plt.show()
            # name = data_csv['image'][i].split('.')[0]
            name = data_csv['image'][i].split('.')[0]
            face_descriptor = facerec.compute_face_descriptor(face_chip, scale)
            save_feature(save_path_train + name, face_descriptor)
            
            # save_feature(save_path + name, face_descriptor)
            data.append(face_descriptor)
            label.append(data_csv['label'][i])
    return np.array(data), np.array(label)      

def face_distance(face_encodings, labels, face_to_compare, tolerance):
    if len(face_encodings) == 0:
        return np.empty((0))

    preds = np.linalg.norm(face_encodings - face_to_compare, axis=1)
    sort_prob = np.argsort(preds)
    predict_labels = []
    labels_voted = []
    for index in range(400):
        predict_labels.append(labels[sort_prob[index]])
    print(predict_labels)
    for i in range(0, 389):
        if predict_labels[i] == -1:
            continue
        for j in range(i + 1, 390):
            if predict_labels[i] == predict_labels[j]:
                predict_labels[j] = -1
        labels_voted.append(predict_labels[i])
        if(len(labels_voted) >= 5):
            break
    print (labels_voted)
    # print (labels)
    if preds[sort_prob[0]] > tolerance:
        """write to file"""
        labels = [1000, labels_voted[0], labels_voted[1], labels_voted[2], labels_voted[3]]
        return labels
    else:
        """write to file"""
        labels = [labels_voted[0], labels_voted[1], labels_voted[2], labels_voted[3], labels_voted[4]]
        return labels
    # if preds[sort_prob[0]] > tolerance:
    #     """write to file"""
    #     labels = [1000, labels[sort_prob[0]], labels[sort_prob[1]], labels[sort_prob[2]], labels[sort_prob[3]]]
    #     return labels
    # else:
    #     """write to file"""
    #     labels = [labels[sort_prob[0]], labels[sort_prob[1]], labels[sort_prob[2]], labels[sort_prob[3]], labels[sort_prob[4]]]
    #     return labels


def compare_faces(known_face_encodings, labels, face_encoding_to_check, tolerance=0.6):
    return face_distance(known_face_encodings, labels, face_encoding_to_check, tolerance)


def predict_test(X_train, y_train):
    if os.path.exists(write_result_path):
        os.remove(write_result_path)
        
    print("[+] Predict test data....")
    with open(write_result_path, mode='a') as csv_file:
        fieldnames = ['image', 'label']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
    
    file_names = [os.path.join(image_test_path, f) 
                      for f in os.listdir(image_test_path) if f.endswith(".png")]
    
    for f in file_names:
        image = cv2.imread(f)
        image = cv2.resize(image, (150, 150))
        shape = sp(image, dlib.rectangle(int(0),int(0),int(image.shape[1] - 1),int(image.shape[0] - 1)))
        face_chip = dlib.get_face_chip(image, shape)
        split_name = f.split('/')[-1]
        name = split_name.split('.')[0]
        face_descriptor = facerec.compute_face_descriptor(face_chip, scale)
        save_feature(save_path_test + name, face_descriptor)
        predict_l2 = compare_faces(X_train, y_train, np.array(face_descriptor))
        print(f)
        # split_name = f.split('/')[-1]
        result = str(predict_l2[0]) + ' ' + str(predict_l2[1]) + ' ' + str(predict_l2[2]) + ' '  +  str(predict_l2[3]) + ' ' + str(predict_l2[4])
        with open(write_result_path, mode='a') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow([split_name, result])
            csv_file.close
        # print(split_name)
    print("[+] Predict test data finished, write result to: " + write_result_path)

if __name__=="__main__":
    # frame = cv2.imread(sys.argv[1])
    print("Load data...")
    images, labels = feature_extraction()
    print("Done load data...")
    predict_test(images, labels)
            